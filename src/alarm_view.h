class AlarmView : public AppView
{
public:
   AlarmView(App& app): AppView(app),
   menu(80,  20, 180, 40,   "Menu", !flip_y),
   ok  (80,  60, 180, 80,   "OK",   !flip_y)
   {
      menu.background_color(red);
      ok.background_color(red);
      add_ctrl(menu);
      add_ctrl(ok);
   }
   int max_fps() { return 5; }

   void on_mouse_button_up(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_up(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
      }
   }

   void on_mouse_button_down(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_down(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
      }
   }

   void on_mouse_move(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_move(x, y, (flags & agg::mouse_left) != 0))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }
   }

   void on_ctrl_change()
   {
      app.play_music(app.current_sound, 0);
      if (menu.status())
      {
         menu.status(false);
         app.changeView("menu");
      }
      if (ok.status())
      {
         ok.status(false);
         app.changeView("timer");
      }
   }

   void on_draw()
   {
      pixfmt_type pf(app.rbuf_window());;
      agg::renderer_base<pixfmt_type> rbase(pf);
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;
      ras.reset();
      rbase.clear(black);
      decorator->alarm(ras, sl, rbase);
      agg::render_ctrl(ras, sl, rbase, menu);
      agg::render_ctrl(ras, sl, rbase, ok);
   }

   void enter()
   {
      app.play_music(app.current_sound, 40);
      wait_mode(false);
   }

private:
   void update(long elapsed_time)
   {
      decorator->update(elapsed_time);
   }
   agg::button_ctrl<agg::rgba8> menu;
   agg::button_ctrl<agg::rgba8> ok;
};
